# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Album'
        db.delete_table(u'Api_album')

        # Deleting model 'AlbumLocal'
        db.delete_table(u'Api_albumlocal')


    def backwards(self, orm):
        # Adding model 'Album'
        db.create_table(u'Api_album', (
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('activity', self.gf('django.db.models.fields.BooleanField')()),
        ))
        db.send_create_signal(u'Api', ['Album'])

        # Adding model 'AlbumLocal'
        db.create_table(u'Api_albumlocal', (
            ('title', self.gf('django.db.models.fields.BooleanField')()),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('language', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Api.Language'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'Api', ['AlbumLocal'])


    models = {
        u'Api.language': {
            'Meta': {'object_name': 'Language'},
            'activity': ('django.db.models.fields.BooleanField', [], {}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_default': ('django.db.models.fields.BooleanField', [], {}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['Api']