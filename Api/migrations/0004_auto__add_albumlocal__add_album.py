# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AlbumLocal'
        db.create_table(u'Api_albumlocal', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.BooleanField')()),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('language', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Api.Language'])),
        ))
        db.send_create_signal(u'Api', ['AlbumLocal'])

        # Adding model 'Album'
        db.create_table(u'Api_album', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('activity', self.gf('django.db.models.fields.BooleanField')()),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'Api', ['Album'])


    def backwards(self, orm):
        # Deleting model 'AlbumLocal'
        db.delete_table(u'Api_albumlocal')

        # Deleting model 'Album'
        db.delete_table(u'Api_album')


    models = {
        u'Api.album': {
            'Meta': {'object_name': 'Album'},
            'activity': ('django.db.models.fields.BooleanField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'Api.albumlocal': {
            'Meta': {'object_name': 'AlbumLocal'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Api.Language']"}),
            'title': ('django.db.models.fields.BooleanField', [], {})
        },
        u'Api.language': {
            'Meta': {'object_name': 'Language'},
            'activity': ('django.db.models.fields.BooleanField', [], {}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_default': ('django.db.models.fields.BooleanField', [], {}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['Api']