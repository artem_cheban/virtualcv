#-*- encoding=UTF-8 -*-

from django.db import models
from django.contrib.sites.models import Site
from taggit.managers import TaggableManager
from geoposition.fields import GeopositionField
#from taggit_live.forms import LiveTagField

# Create your models here.


class Language(models.Model):
    title = models.CharField(max_length=20)
    code = models.CharField(max_length=5)
    activity = models.BooleanField()
    is_default = models.BooleanField()
    photo = models.ImageField(upload_to='media/photo/language')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title


class Album(models.Model):
    activity = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    slug = models.SlugField(max_length=200, unique=True, default="")
    cover = models.ImageField(upload_to='media/photo/album_covers', help_text='200*200 image', default="")
    title = models.CharField(max_length=20, default="")
    description = models.CharField(max_length=30, default="")
    tag = TaggableManager()

    class Meta:
        ordering = ['title']
        verbose_name = 'Album'
        verbose_name_plural = 'Albums'

    def __unicode__(self):
        return self.title

    def admin_thumbnail(self):
        if (self.cover) and hasattr(self.cover, 'url'):
            return u'<img src="/%s" width="150" height="100"/>' % (self.cover.url)
        return u'<img src="media/default.jpg"  >'
    admin_thumbnail.short_description = 'Thumbnail'
    admin_thumbnail.allow_tags = True


class Photo(models.Model):
    image = models.ImageField(upload_to='photo/photos')
    album = models.ForeignKey(Album)
    activity = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=30)
    description = models.CharField(max_length=50)

    def __unicode__(self):
        return self.title

    def admin_thumbnail(self):
        if (self.image) and hasattr(self.image, 'url'):
            return u'<img src="/%s"  width="100" height="100"/>' % (self.image.url)
        return u'<img src="/media/default.jpg">'

    admin_thumbnail.short_description = 'Thumbnail'
    admin_thumbnail.allow_tags = True



class FlatPage(models.Model):
    url = models.CharField(max_length=100)
    title = models.CharField(max_length=200)
    content = models.TextField()
    enable_comments = models.BooleanField()
    template_name = models.CharField(max_length=70, blank=True)
    registration_required = models.BooleanField()
    sites = models.ManyToManyField(Site, related_name='sites')
    tag = TaggableManager()

    def __unicode__(self):
        return self.title

class FlatPagePhoto(models.Model):
    image = models.ImageField(upload_to='photo/flatpagephoto')
    flatpage = models.ForeignKey(FlatPage)
    activity = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=30)
    description = models.CharField(max_length=50)

    def __unicode__(self):
        return self.title

    def admin_thumbnail(self):
       if (self.image) and hasattr(self.image, 'url'):
           return u'<img src="/%s" />' % (self.image.url)
       return u'<img src="/media/default.jpg">'


class Event(models.Model):
    title = models.CharField(max_length=50)
    activity = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    content = models.TextField()
    tag = TaggableManager()

    def __unicode__(self):
        return self.title


class EventPhoto(models.Model):
    image = models.ImageField(upload_to='photo/event')
    event = models.ForeignKey(Event)
    activity = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=30)
    description = models.CharField(max_length=50)

    def __unicode__(self):
        return self.title

    def admin_thumbnail(self):
       if (self.image) and hasattr(self.image, 'url'):
           return u'<img src="/%s" />' % (self.image.url)
       return u'<img src="/media/default.jpg">'

class PlaceCategory(models.Model):
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return self.title

class Place(models.Model):
    title = models.CharField(max_length=50)
    position = GeopositionField()
    activity = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    content = models.TextField()
    tag = TaggableManager()
    category = models.ForeignKey(PlaceCategory)

    def __unicode__(self):
        return self.title

class PlacePhoto(models.Model):
    image = models.ImageField(upload_to='photo/place')
    place = models.ForeignKey(Place)
    activity = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=30)
    description = models.CharField(max_length=50)

    def __unicode__(self):
        return self.title

    def admin_thumbnail(self):
       if (self.image) and hasattr(self.image, 'url'):
           return u'<img src="/%s" />' % (self.image.url)
       return u'<img src="/media/default.jpg">'



