from modeltranslation.translator import translator, TranslationOptions
from Api.models import Album, Photo, FlatPage, FlatPagePhoto, Event, EventPhoto, Place, PlaceCategory, PlacePhoto

class AlbumTranslationOptions(TranslationOptions):
    fields = ('title', 'description',)

translator.register(Album, AlbumTranslationOptions)

class PhotoTranslationOptions(TranslationOptions):
    fields = ('title', 'description',)

translator.register(Photo, PhotoTranslationOptions)

class FlatPageTranslationOptions(TranslationOptions):
    fields = ('title', 'content',)

translator.register(FlatPage, FlatPageTranslationOptions)

class FlatPagePhotoTranslationOptions(TranslationOptions):
    fields = ('title', 'description',)

translator.register(FlatPagePhoto, FlatPagePhotoTranslationOptions)

class EventTranslationOptions(TranslationOptions):
    fields = ('title', 'content',)

translator.register(Event, EventTranslationOptions)

class EventPhotoTranslationOptions(TranslationOptions):
    fields = ('title', 'description',)

translator.register(EventPhoto, EventPhotoTranslationOptions)

class PlaceCategoryTranslationOptions(TranslationOptions):
    fields = ('title', 'description',)

translator.register(PlaceCategory, PlaceCategoryTranslationOptions)

class PlaceTranslationOptions(TranslationOptions):
    fields = ('title', 'content',)

translator.register(Place, PlaceTranslationOptions)

class PlacePhotoTranslationOptions(TranslationOptions):
    fields = ('title', 'description',)

translator.register(PlacePhoto, PlacePhotoTranslationOptions)