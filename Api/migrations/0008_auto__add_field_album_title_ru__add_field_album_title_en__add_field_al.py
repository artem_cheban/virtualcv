# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Album.title_ru'
        db.add_column(u'Api_album', 'title_ru',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=20, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Album.title_en'
        db.add_column(u'Api_album', 'title_en',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=20, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Album.title_ua'
        db.add_column(u'Api_album', 'title_ua',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=20, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Album.description_ru'
        db.add_column(u'Api_album', 'description_ru',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=30, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Album.description_en'
        db.add_column(u'Api_album', 'description_en',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=30, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Album.description_ua'
        db.add_column(u'Api_album', 'description_ua',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=30, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Album.title_ru'
        db.delete_column(u'Api_album', 'title_ru')

        # Deleting field 'Album.title_en'
        db.delete_column(u'Api_album', 'title_en')

        # Deleting field 'Album.title_ua'
        db.delete_column(u'Api_album', 'title_ua')

        # Deleting field 'Album.description_ru'
        db.delete_column(u'Api_album', 'description_ru')

        # Deleting field 'Album.description_en'
        db.delete_column(u'Api_album', 'description_en')

        # Deleting field 'Album.description_ua'
        db.delete_column(u'Api_album', 'description_ua')


    models = {
        u'Api.album': {
            'Meta': {'object_name': 'Album'},
            'activity': ('django.db.models.fields.BooleanField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '30'}),
            'description_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'description_ua': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '30', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20'}),
            'title_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'title_ua': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'Api.language': {
            'Meta': {'object_name': 'Language'},
            'activity': ('django.db.models.fields.BooleanField', [], {}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_default': ('django.db.models.fields.BooleanField', [], {}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['Api']