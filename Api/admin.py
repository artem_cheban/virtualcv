from django.contrib import admin
from Api.models import Language, Album, Photo, FlatPage, FlatPagePhoto, Event, EventPhoto, PlacePhoto, PlaceCategory, Place
from modeltranslation.admin import TabbedTranslationAdmin
from grappelli_modeltranslation.admin import TranslationAdmin

# Register your models here.


class LanguageAdmin(admin.ModelAdmin):
    list_display = ('title', 'code', 'photo', 'activity', 'created_at', 'updated_at', 'is_default')


class AlbumAdmin(TranslationAdmin, admin.ModelAdmin):
    list_display = ['title', 'admin_thumbnail', 'title', 'description']
    class Media:
         css = {'all': ('/jquery-ui-1.11.1.custom/jquery-ui.css',
                        )
               }
         js = (
            '/jquery-ui-1.11.1.custom/external/jquery/jquery.js',
            '/jquery-ui-1.11.1.custom/jquery-ui.js',
            )


class PhotoAdmin(TranslationAdmin, admin.ModelAdmin):
    list_display = ['title', 'album', 'admin_thumbnail']
    list_filter = ['album']


class FlatPageAdmin (TabbedTranslationAdmin, admin.ModelAdmin):
    list_display = ['title', 'url']

class FlatPagePhotoAdmin(TranslationAdmin, admin.ModelAdmin):
    list_display = ['title', 'flatpage', 'admin_thumbnail']
    list_filter = ['flatpage']


class EventAdmin(TranslationAdmin, admin.ModelAdmin):
    list_display = ['title', 'activity', 'created_at', 'tag']
    list_filter = ['created_at']

class EventPhotoAdmin(TranslationAdmin, admin.ModelAdmin):
    list_display = ['title', 'event', 'admin_thumbnail']
    list_filter = ['event']

class PlaceAdmin(TranslationAdmin, admin.ModelAdmin):
    list_display = ['title', 'x_coord', 'y_coord', 'content']
    list_filter = ['title',]

class PlaceCategoryAdmin(TranslationAdmin, admin.ModelAdmin):
    list_filter = ['title',]

class PlacePhotoAdmin(TranslationAdmin, admin.ModelAdmin):
    list_filter = ['title',]


admin.site.register(Language, LanguageAdmin)
admin.site.register(Album, AlbumAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(FlatPage, FlatPageAdmin)
admin.site.register(FlatPagePhoto, FlatPagePhotoAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(EventPhoto, EventPhotoAdmin)
admin.site.register(PlaceCategory, PlaceCategoryAdmin)
admin.site.register(Place, PlacePhotoAdmin)
admin.site.register(PlacePhoto, PlacePhotoAdmin)