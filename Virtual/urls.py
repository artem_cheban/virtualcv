from django.conf.urls import patterns, include, url
from django.contrib.auth.models import User
from django.views.generic import ListView, DetailView
from Api.models import Album, Photo
import os.path
from django.conf import settings
from django.conf.urls.static import static


from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    #
    # url(r'^blog/', include('blog.urls')),
    (r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'Api.views.index', name='index'),
    url(r'^simple/', 'Api.views.simple', name='simple'),
    url(r'', include('taggit_live.urls')),
    url(r'^albums/', 'Api.views.albums'),
    url(r'^new_albums/(\d{1,10})', 'Api.views.new_albums'),
    url(r'^album_photos/(\d{1,2})', 'Api.views.album_photos'),
    url(r'^album_front_photos/(\d{1,2})', 'Api.views.album_front_photos'),
    url(r'^new_photos/(\d{1,10})', 'Api.views.new_photos'),
    url(r'^album/(\d{1,2})', 'Api.views.album',name='album'),
    url(r'^events/', 'Api.views.events'),
    url(r'^event/(\d{1,2})', 'Api.views.event',name='event'),
    url(r'^new_event_photos/(\d{1,10})', 'Api.views.new_event_photos'),
    url(r'^flatpages/', 'Api.views.flatpages'),
    url(r'^flatpage_photos/', 'Api.views.flatpage_photos'),
    url(r'^new_flatpage_photos/(\d{1,10})', 'Api.views.new_flatpage_photos'),
    url(r'^new_flatpages/(\d{1,10})', 'Api.views.new_flatpages'),
    url(r'^places/', 'Api.views.places'),
    url(r'^place_photos/', 'Api.views.place_photos'),
    url(r'^place_categories/', 'Api.views.place_categories'),
    url(r'^new_places/(\d{1,10})', 'Api.views.new_places'),
    url(r'^new_place_photos/(\d{1,10})', 'Api.views.new_place_photos'),
    url(r'^new_place_categories/(\d{1,10})', 'Api.views.new_place_categories'),


)


urlpatterns += patterns('',
    # ... the rest of your URLconf goes here ...
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



