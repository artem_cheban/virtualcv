from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response
import json
from Api.models import *
from django.core import serializers
import datetime
from django.core.urlresolvers import reverse



# Create your views here.


def index(request):
    return HttpResponse('Hi world')

def simple(request):
    return render_to_response('flat.html')

# ALBUMS

def albums(request):
    response_data = {}
    response_data = list(Album.objects.all())
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type="application/json")

def new_albums(request, time):
    try:
        time = int(time)
    except ValueError:
        raise Http404()
    dt = datetime.datetime.fromtimestamp(time)
    response_data = {}
    response_data = list(Album.objects.filter(updated_at__gte=dt))
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type="application/json")

def album(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    response_data = {}
    response_data = list(Album.objects.filter(id=offset))
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type="application/json")

def album_photos(request, album_id):
    try:
        album_id = int(album_id)
    except ValueError:
        raise Http404()
    response_data = {}
    response_data = list(Photo.objects.filter(album_id=album_id))
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type="application/json")

def photos(request):
    response_data = {}
    response_data = list(Photo.objects.all())
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type="application/json")

def new_photos(request, time):
    try:
        time = int(time)
    except ValueError:
        raise Http404()
    dt = datetime.datetime.fromtimestamp(time)
    response_data = {}
    response_data = list(Photo.objects.filter(updated_at__gte=dt))
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type="application/json")

# EVENTS

def events(request):
    response_data = {}
    response_data = list(Event.objects.all())
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type="application/json")

def new_events(request, time):
    try:
        time = int(time)
    except ValueError:
        raise Http404()
    time = datetime.datetime.fromtimestamp(time)
    response_data = {}
    response_data = list(Event.objects.filter(updated_at__gte=time))
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type='application/json')

def event(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    response_data = {}
    response_data = list(Event.objects.filter(id=offset))
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type="application/json")

def event_photos(request):
    response_data = {}
    response_data = list(EventPhoto.objects.all())
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type="application/json")

def new_event_photos(request, time):
    try:
        time = int(time)
    except ValueError:
        raise Http404()
    time = datetime.datetime.fromtimestamp(time)
    response_data = {}
    response_data = list(EventPhoto.objects.filter(updated_at__gte=time))
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type='application/json')

# FLAT PAGE

def flatpages(request):
    response_data = {}
    response_data = list(FlatPage.objects.all())
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type="application/json")

def flatpage_photos(request):
    response_data = {}
    response_data = list(FlatPagePhoto.objects.all())
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type="application/json")

def new_flatpages(request, time):
    try:
        time = int(time)
    except ValueError:
        raise Http404()
    time = datetime.datetime.fromtimestamp(time)
    response_data = {}
    response_data = list(FlatPage.objects.filter(updated_at__gte=time))
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type='application/json')

def new_flatpage_photos(request, time):
    try:
        time = int(time)
    except ValueError:
        raise Http404()
    time = datetime.datetime.fromtimestamp(time)
    response_data = {}
    response_data = list(FlatPagePhoto.objects.filter(updated_at__gte=time))
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type='application/json')

# PLACES

def places(request):
    response_data = {}
    response_data = list(Place.objects.all())
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type="application/json")

def place_photos(request):
    response_data = {}
    response_data = list(PlacePhoto.objects.all())
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type="application/json")

def place_categories(request):
    response_data = {}
    response_data = list(PlaceCategory.objects.all())
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type="application/json")

def new_places(request, time):
    try:
        time = int(time)
    except ValueError:
        raise Http404()
    time = datetime.datetime.fromtimestamp(time)
    response_data = {}
    response_data = list(Place.objects.filter(updated_at__gte=time))
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type='application/json')

def new_place_photos(request, time):
    try:
        time = int(time)
    except ValueError:
        raise Http404()
    time = datetime.datetime.fromtimestamp(time)
    response_data = {}
    response_data = list(PlacePhoto.objects.filter(updated_at__gte=time))
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type='application/json')

def new_place_categories(request, time):
    try:
        time = int(time)
    except ValueError:
        raise Http404()
    time = datetime.datetime.fromtimestamp(time)
    response_data = {}
    response_data = list(PlaceCategory.objects.filter(updated_at__gte=time))
    data = serializers.serialize('json', response_data)
    return HttpResponse(json.dumps(data), content_type='application/json')

#for commit 2


def album_front_photos(request, album):
    try:
        album_id = int(album)
    except ValueError:
        raise Http404()
    pictures = Photo.objects.filter(album_id = album_id)

    return render_to_response('pictures.html', locals())




